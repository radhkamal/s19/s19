/*
	Activity:
	1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
	2. Link the script.js file to the index.html file.

*/

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let getCube = (num) => {

	let cube = num ** 3;
	console.log(`The cube of ${num} is ${cube}`);
}

getCube(4);


// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

const address = ["Marina Bay Sands", "10 Bayfront Avenue", "Marina Bay", "Singapore City", "018971", "Singapore"];

let [building, street, district, city, postalCode, country] = address;

console.log(`I live at ${building}, ${street}, ${district}, ${city} ${postalCode}, ${country}.`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

let animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: 1075,
	length: '20 feet 3 inches'
}

const {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length}.`);

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

let array = [11, 65, 13, 17, 62, 38, 95, 96, 302, 21, 22];

array.forEach((element) => {
	console.log(element);
})


// 11. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 12. Create/instantiate a new object from the class Dog and console log the object.

class Dog {
	constructor(name, age, breed) {
			this.name = name;
			this.age = age;
			this.breed = breed;		
	}
}

const myDog = new Dog("Luna", "2", "Miniature Schnauzer");
console.log(myDog);